$db_user = 'naizheng'
$db_passwd='AComplexPasswd'

package {
    'unzip':
    ensure => 'installed',
}
package {
    'python3-pip':
    ensure => 'installed',
}

package {
    "postgresql-12":
    ensure => "installed"
}

package {
    'flask':
    provider => 'pip3',
    ensure => 'installed',
}

firewall {'100 Allow ssh access':
 dport => [22,43,8080],
 action => accept,
}

class { 'postgresql::server':
}

postgresql::server::db { 'mydatabasename':
  user     =>  $db_user,
  password => postgresql::postgresql_password($db_user, $db_passwd),
}
